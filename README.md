# Job Interview Coding Task

This is a coding task/test to analyse a graph representation of a network build so that it can be costed.  Details are in the word document [./documents/Challenge.docx](./documents/Challenge.docx).  Also in the [documents](documents) folder are two examples of graph definitions.

## Approach

When completing this task there were three separate stages:

1. **Research**: Understand a little about GraphML and find some libraries that can parse it and maybe do a little graph analysis.  I chose the GraphML style as it felt more formal and structured and had better documentation through Google searching.  I found two main libraries when looking about; `igraph` is a powerful C based graph analysis tool with a python interface that could have saved me a little mathematics but has significant setup overheads;  the choice made was `pygraphml` a simple python only GraphML parser that results in am object collection representing the nodes and edges of the graph.
2. **Mathematics**: A bit of pencil and paper thinking to work out what the mathematics are needed. In the main it is a case of total this and multiply by that.  However for pot calculations I also need to work out how to walk that graph.  A breadth first brute force walk shows me all possible routes to all possible end points, by discarding all routes whose endpoint is not the target and all routes that are cyclic leaves only valid routes.  This requires a recursive function.
3. **Coding**: With a mathematical design in place, I wrote two classes, one to analyse the graph, the other to cost that analysis based on rates. The rate card only appear as a table in the word doc, so I made up a `JSON` format for capturing them.  As rates may change regularly and new vendors may offer services these `JSON` representations are kept in separate files to aid editing and extension.

## Assumptions

**Story Assumptions**

- Acceptance Criteria for this story only ask about total costs, if greater break down is required then that is separate story
- Rate Cards show items having a cost per item *or* cost per meter with only pots have a combination of both.  If calculations on those rates change that would be a separate story.
- This is a command line tool and results will be copied in to other application for comparison.
- Many build projects may need to costed at one time against many rates cards.  The script will work against a directory of problem files and a directory of rates cards.

**Graph Assumptions**

- Graph will be simple/strict graph
- There will be no loop, multi or hyper edges
- There will always be at least one cabinet
- There will always be at least one pot
- All nodes will be connected

**Calculation Assumptions**

- Where two different paths from pot to cabinet exist and have the same distance then route is not material.
- Currency is only sterling

## Usage

### Requirements

- Python 3.5+
- pygraph 2.2+

###Installation and Execution

- Clone the repository
```bash
$ git clone 
```
- Install the Requirements
```bash
$ pip install -r requirements.txt
```
- Run the script
```bash
$ python buildCosts.py --graph_path /dir/containing/graphs/ --rates_path /dir/contiaing/rates/
```
### Options

```
-g, --graph_path GRAPH_PATH The absolute or relative path to the GraphML problem files
-r, --rates_path RATES_PATH The absolute or relative path to the JSON rates cards
```

Each of these paths is for a directory containing either problem files in GraphML format or rate cards in the JSON format below.

### Rates JSON

Specified by example, place this json in a file named <<RateName.json>> and place the file in the rates_path:

```json
{
  "Cabinet":{
    "costPerItem": 1000
  },
  "Chamber": {
    "costPerItem": 200
  },
  "Pot":{
    "costPerItem": 100,
    "costPerMeter": 0
  },
  "roadTrench": {
    "costPerMeter": 100
  },
  "vergeTrench": {
    "costPerMeter": 50
  }
}
```

## Limitations

**Actual**

- all files in `rates_path` must be rates files
- all files in `graph_path` must be graph/problem files
- graph analysis coupled to `pygraphml` module
- hard coded string keys and values from problem files and rate files (coupling again)
- no tests - would not know how

**Assumed**

- will probably grind to a halt with large files or large numbers of files