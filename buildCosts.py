# -*- coding: utf-8 -*-
import pygraphml
import json
import os
import argparse


class GraphAnalysis:
    """Analyse a valid GraphML object collection to calculate and store its properties."""
    __graph = None
    __cabinetCount = 0
    __chamberCount = 0
    __potCount = 0
    __roadDistance = 0
    __vergeDistance = 0
    __potDistance = 0

    def __init__(self, graph_file_name, parser):
        """Load a GraphML file and analyse it."""
        graph = parser.parse(graph_file_name)
        self.__graph = graph
        self.__count_cabinets_chambers_pots()
        self.__measure_tunnels()
        self.__calculate_pot_distances()

    def get_cabinet_count(self):
        """Return an integer count of the cabinets in the graph."""
        return self.__cabinetCount

    def get_chamber_count(self):
        """Return an integer count of the chambers in the graph."""
        return self.__chamberCount

    def get_pot_count(self):
        """Return an integer count of the pots in the graph."""
        return self.__potCount

    def get_road_distance(self):
        """Return the integer total road distance in the graph."""
        return self.__roadDistance

    def get_verge_distance(self):
        """Return integer total verge distance in the graph."""
        return self.__vergeDistance

    def get_pot_distance(self):
        """Return the integer sum of the distance of each pot to its nearest cabinet."""
        return self.__potDistance

    def __count_cabinets_chambers_pots(self):
        """Count the number of cabinets, chambers and pots in the graph and store them as a property."""
        cabinets = 0
        chambers = 0
        pots = 0
        for node in self.__graph.nodes():
            if node['d0'] == 'Cabinet':
                cabinets += 1
            if node['d0'] == 'Chamber':
                chambers += 1
            if node['d0'] == 'Pot':
                pots += 1
        self.__cabinetCount = cabinets
        self.__chamberCount = chambers
        self.__potCount = pots

    def __measure_tunnels(self):
        """Sum the total distance of road and verge in the graph and store them as a property."""
        roads = 0
        verges = 0
        for edge in self.__graph.edges():
            if edge['d1'] == 'verge':
                verges += int(edge['d2'])
            if edge['d1'] == 'road':
                roads += int(edge['d2'])
        self.__roadDistance = roads
        self.__vergeDistance = verges

    def __routes_between_points(self, this_node, finish_node, paths):
        """Find possible routes between this node and the target node, return a list of dicts of path and distance"""
        for path in paths:
            if path['path'].endswith(this_node['label']):
                if this_node['label'] == finish_node['label']:
                    continue
                for edge in this_node.edges():
                    if edge.node(this_node)['label'] in path['path']:
                        continue
                    if edge.node(this_node)['d0'] == 'Pot':
                        continue
                    if (edge.node(this_node)['d0'] == 'Cabinet' and
                            edge.node(this_node)['label'] != finish_node['label']):
                        continue
                    next_paths = self.__routes_between_points(
                        edge.node(this_node),
                        finish_node,
                        [{'path': path['path'] + edge.node(this_node)['label'], 'dist': path['dist'] + int(edge['d2'])}]
                    )
                    for next_path in next_paths:
                        if next_path['path'].endswith(finish_node['label']):
                            paths.extend([next_path])
        return paths

    def __calculate_pot_distances(self):
        """For each pot find its nearest cabinet and sum the distances and store it in a property"""
        total_distance = 0
        for node in self.__graph.nodes():
            node_dist = None
            if node['d0'] == 'Pot':
                for target in self.__graph.nodes():
                    if target['d0'] == 'Cabinet':
                        possible_paths = self.__routes_between_points(
                            node,
                            target,
                            [{'path': node['label'], 'dist': 0}]
                        )
                        for path in possible_paths:
                            if not (path['path'].endswith(target['label'])):
                                continue
                            if node_dist is None:
                                node_dist = path['dist']
                            else:
                                node_dist = min(node_dist, path['dist'])
            if not (node_dist is None):
                total_distance += node_dist
        self.__potDistance = total_distance


class CostAnalysis:
    """Calculate total cost based on graph analysis and rate card."""
    __graphAnalysis = None
    __ratesJson = None
    __cabinetCost = 0
    __chamberCost = 0
    __potCost = 0
    __roadTrenchCost = 0
    __vergeTrenchCost = 0
    __totalCost = 0

    def __init__(self, graph_analysis, rates_json):
        """Calculate cost of graph given rate card."""
        self.__graphAnalysis = graph_analysis
        self.__ratesJson = rates_json
        self.__cost_cabinets_chambers()
        self.__cost_trenches()
        self.__cost_pots()
        self.__total_costs()

    def get_total_cost(self):
        """Return the integer total cost of the graph."""
        return self.__totalCost

    def __cost_cabinets_chambers(self):
        """Calculate cost per unit of cabinets and chambers and store in property"""
        self.__cabinetCost = self.__graphAnalysis.get_cabinet_count() * self.__ratesJson['Cabinet']['costPerItem']
        self.__chamberCost = self.__graphAnalysis.get_chamber_count() * self.__ratesJson['Chamber']['costPerItem']

    def __cost_pots(self):
        """Calculate cost per unit and cost per meter for post store in property"""
        self.__potCost = (
            (self.__graphAnalysis.get_pot_distance() *
             self.__ratesJson['Pot']['costPerMeter']) +
            (self.__graphAnalysis.get_pot_count() *
             self.__ratesJson['Pot']['costPerItem'])
        )

    def __cost_trenches(self):
        """Calculate cost per meter of road and verge trenches and store in property"""
        self.__roadTrenchCost = (
            self.__graphAnalysis.get_road_distance() *
            self.__ratesJson['roadTrench']['costPerMeter']
        )
        self.__vergeTrenchCost = (
            self.__graphAnalysis.get_verge_distance() *
            self.__ratesJson['vergeTrench']['costPerMeter']
        )

    def __total_costs(self):
        """Sum all part costs for total cost of graph and store in property"""
        self.__totalCost = (
            self.__cabinetCost +
            self.__chamberCost +
            self.__potCost +
            self.__roadTrenchCost +
            self.__vergeTrenchCost
        )


def main(graph_path, rates_path):
    """Control manipulation of files and out put results"""
    graph_parser = pygraphml.GraphMLParser()
    costs = {}
    for root, dirs, graphNames in os.walk(graph_path):
        for graphName in graphNames:
            this_graph = GraphAnalysis(os.path.join(root, graphName), graph_parser)
            for root2, dirs2, rateNames in os.walk(rates_path):
                for rateName in rateNames:
                    this_rate = json.load(open(os.path.join(root2, rateName)))
                    graph_name_str = graphName.split(".")
                    rate_name_str = rateName.split(".")
                    if not (graph_name_str[0] in costs):
                        costs[graph_name_str[0]] = {}
                    costs[graph_name_str[0]][rate_name_str[0]] = CostAnalysis(this_graph, this_rate)

    for graph, rates in costs.items():
        for rate in rates:
            print(graph, rate, "£" + str(costs[graph][rate].get_total_cost()))


if __name__ == "__main__":
    options_parser = argparse.ArgumentParser()
    options_parser.add_argument(
        "-g", "--graph_path",
        default="./graphs/",
        type=str,
        help="The absolute or relative path to the GraphML problem files"
    )
    options_parser.add_argument(
        "-r", "--rates_path",
        default="./rates/",
        type=str,
        help="The absolute or relative path to the JSON rates cards"
    )
    options = options_parser.parse_args()
    main(options.graph_path, options.rates_path)
